import * as React from 'react';
import {Text, TouchableOpacity} from 'react-native'

const button = ({text, color, data}) => {
    React.useEffect(() => {
        console.log('This is Button.js')
        console.log(text)
    }, [])
    return (
        <>
            <TouchableOpacity style={{backgroundColor: color}} onPress={() => console.log(data)}>
                <Text>
                    {text}
                </Text>
            </TouchableOpacity>
        </>
    )
}

export default button;