import {connect} from 'react-redux';
import Home from './Home';

const mapStateToProps = (state) => ({
    token: state.auth.token
});

// redux store in here
const mapDispatchToProps = (dispatch) => {
  return {
    // FetchToken: (token) => dispatch(auth(token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
