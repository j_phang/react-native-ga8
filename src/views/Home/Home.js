import * as React from 'react';
// import PropTypes from 'prop-types';
import {View, Text, TextInput} from 'react-native';
import Button from '../../components/Button'

const propTypes = {};
const defaultProps = {};

const Home = (props) => {
    /*
        constructor(props){
            super(props)
            this.state = {
                username: '',
                password: ''
            }
        }
    */

    let interval;

    const [username, setUsername] = React.useState(''); //Deklarasi State
    const [password, setPassword] = React.useState(''); //Deklarasi State

    // Life Cycle

    // ComponentDidMount()
    React.useEffect(() => {
        console.log('Hello')
        console.log(props)

        interval = setInterval(() => {
            console.log('Hello')
        }, 5000)

        setTimeout(() => {
            clearInterval(interval)
            console.log('Interval has been Cleared')
        }, 10000)

        // ComponentWillUnmount(){ clearInterval(interval) }
        return () => {
            console.log('Hello')
        }
    }, []) // Isi [] dengan State yang telah kalian buat, agar fungsi ini dipanggil berulang ( Seperti ComponentDidUpdate() )

    React.useEffect(() => {
        console.log('This is UserName')
    }, [username])

    React.useEffect(() => {
        console.log('This is Password')
    }, [password])

    React.useEffect(() => {
        console.log('This is Always updating')
    }) // ComponentDidUpdate()

    return (
        <View>
            <Text>Home</Text>
            <TextInput onChangeText={(text) => setUsername(text)} value={username} />
            {/*
                onChangeText={text => this.setState({ username: text })}
            */}
            <TextInput onChangeText={(text) => setPassword(text)} value={password} />
            <Button color='#FFF' text='Hello Button' data={{username, password}}/>
        </View>
    );
};

Home.propTypes = propTypes;
Home.defaultProps = defaultProps;

export default Home;
