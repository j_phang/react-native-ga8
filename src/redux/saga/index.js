import { takeLatest, all } from "@redux-saga/core/effects";
import { loginApps } from './user'

export default function* IndexSaga() {
    yield all([
        takeLatest('FETCH_TOKEN', loginApps)
    ])
}
