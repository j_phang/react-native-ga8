import axios from 'axios';
import api from './index.js';

export const login = async data => {
    const response = await axios.post('https://backendtodo101.herokuapp.com/user/login', data)
    // const response = await api.post('/user/login', data)
    console.log(response)

    return response.data.token
};

export const register = async data => {
    const response = await api.post('/user/register', data)

    return response
}
