import { put, call } from 'redux-saga/effects'
import { login } from './api/userAPI';
export function* loginApps(data) {
    try {
        const token = yield call(login, data)
        yield put({ type: 'SHOW_USER_DATA', payload: token })
        yield put({ type: 'SUCCESS_USER_DATA', payload: 'Successfully get' })
    } catch (error) {
        yield put({ type: 'FAILED_GET_USER_DATA', payload: 'Fatal ERROR' })
        console.error(error)
    }
}
