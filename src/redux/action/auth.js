export const auth = data => {
    return {
        type: 'FETCH_TOKEN',
        payload: data
    };
};